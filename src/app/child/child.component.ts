import { Component } from '@angular/core';
import {Observable, of} from 'rxjs';
import {delay, tap} from 'rxjs/operators';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent {
  public preloaded = false;

  public preload(): Observable<any> {
    return of({}).pipe(
      delay(1000),
      tap(_ => this.preloaded = true)
    );
  }
}
