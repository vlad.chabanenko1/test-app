import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ParentComponent} from './parent/parent.component';
import {ParentWithFirstSolutionComponent} from './parent-with-first-solution/parent-with-first-solution.component';

const routes: Routes = [
  {
    path: 'first-parent',
    component: ParentComponent,
  },
  {
    path: 'second-parent',
    component: ParentWithFirstSolutionComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
