import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ParentComponent} from './parent/parent.component';
import {ChildComponent} from './child/child.component';
import {LoaderComponent} from './loader/loader.component';
import {ParentWithFirstSolutionComponent} from './parent-with-first-solution/parent-with-first-solution.component';

@NgModule({
  declarations: [
    AppComponent,
    LoaderComponent,
    ParentComponent,
    ChildComponent,
    ParentWithFirstSolutionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
