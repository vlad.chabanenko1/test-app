import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {ChildComponent} from '../child/child.component';
import {forkJoin, of} from 'rxjs';

@Component({
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit, AfterViewInit {
  @ViewChild('child', { static: false }) public child: ChildComponent;
  public showLoader = false;

  constructor(private cdf: ChangeDetectorRef) {
  }

  public ngOnInit(): void {
    // Я не могу написать этот код сдесь. Причина:
    // TypeError: Cannot read property 'preload' of undefined
    // const request$ = of({});
    // this.showLoader = true;
    // forkJoin([this.child.preload(), request$]).subscribe(_ => {
    //   this.showLoader = false;
    // });
  }

  public ngAfterViewInit() {
    // Я не могу написать этот код сдесь. Причина:
    // ERROR Error: ExpressionChangedAfterItHasBeenCheckedError: Expression has changed after it was checked. Previous value: 'show: false'. Current value: 'show: true'.
    // const request$ = of({});
    // this.showLoader = true;
    // forkJoin([this.child.preload(), request$]).subscribe(_ => {
    //   this.showLoader = false;
    // });

    // А это костыль, который не очень хочется делать.
    // this.cdf.detectChanges();
  }
}
