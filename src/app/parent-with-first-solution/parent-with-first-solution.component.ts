import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {ChildComponent} from '../child/child.component';
import {forkJoin, Observable, of, Subject} from 'rxjs';

@Component({
  templateUrl: './parent-with-first-solution.component.html',
  styleUrls: ['./parent-with-first-solution.component.css']
})
export class ParentWithFirstSolutionComponent implements OnInit, AfterViewInit {
  @ViewChild('child', { static: false }) public child: ChildComponent;
  private childPreloaded$ = new Subject<any>();
  public showLoader = false;

  constructor() {
  }

  public ngOnInit(): void {
    const request$ = of({});
    this.showLoader = true;
    forkJoin([this.childPreloaded$, request$]).subscribe(_ => {
      this.showLoader = false;
    });
  }

  public ngAfterViewInit() {
    this.child.preload().subscribe(_ => {
      this.childPreloaded$.next({});
      this.childPreloaded$.complete();
    });
  }
}
